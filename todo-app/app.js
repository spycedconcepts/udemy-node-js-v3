const chalk = require('chalk');
const yargs = require('yargs');

const { getNotes } = require('./notes');

//customize yargs version#
yargs.version('1.1.0');

//add, remove, read, list, edit, help
yargs.command({
  command: 'add',
  describe: 'Add a new note',
  builder: {
    title: {
      describe: 'Note title.',
      demandOption: true,
      type: 'string'
    },
    body: {
      describe: 'Note body.',
      demandOption: true,
      type: 'string'
    }
  },
  handler: function (argv) {
    console.log('Title: ', argv.title, '\nBody: ', argv.body);
  }
});

yargs.command({
  command: 'read',
  describe: 'Reads a note',
  handler: function() {
    console.log('Reading a specific note');
  }
});

yargs.command({
  command: 'list',
  describe: "Lists all available notes",
  handler: function() {
    console.log('Listing all notes')
  }
})

yargs.command({
  command: 'remove',
  describe: 'Removes and existing note',
  handler: function () {
    console.log('Removing a note');
  }
});

yargs.parse();
