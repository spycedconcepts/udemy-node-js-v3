const fs = require('fs');

// const book = {
//   title: 'Ego is the enemy',
//   author: 'Ryan Holiday'
// };

// bookJSON = JSON.stringify(book);

// fs.writeFileSync('1-json.json', bookJSON);

// const dataBuffer = fs.readFileSync('1-json.json');
// const dataJSON = dataBuffer.toString();
// const data = JSON.parse(dataJSON);
// console.log(data.title);

// CHALLENGE
//==========

const getData = () => {
  const dataBuffer = fs.readFileSync('data.json');
  const dataJSON = dataBuffer.toString();
  return JSON.parse(dataJSON);
}
let data = getData();

console.log(`${data.name}, aged ${data.age}, of planet ${data.planet}`);

data.name = "Stuart";
data.age = "50";

fs.writeFileSync('data.json', JSON.stringify(data));

data = getData();
console.log(`${data.name}, aged ${data.age}, of planet ${data.planet}`);

